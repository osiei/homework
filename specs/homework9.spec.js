import { expect, test } from '@jest/globals';
import { mail, mailArray } from "../framework/builder/testEmail";
import { user } from '../framework/config/user'
import { apiProvider } from '../framework';


describe('Проверки почтовых ящиков через сервис mailboxlayer.', () => {
    test('Положительный кейс', async () => {
        const request = await apiProvider().checkMail().checkEmailmethod(mail.positiveMail, user.mailboxlayer.access_key);
        expect(request.status).toEqual(200);
        expect(request.body.format_valid).toEqual(true);
    });

    test.each(mailArray)
    ('check mail %s === %s', async ( mail, expected ) => {
        const r = await apiProvider().checkMail().checkEmailmethod(mail, user.mailboxlayer.access_key);
        expect(r.status).toEqual(200);
        expect(r.body.format_valid).toEqual(expected);
    });

    test('Проверка доступа без API key', async () => {
        const r = await apiProvider().checkMail().checkEmailmethod(mail.positiveMail);
        expect(r.status).toEqual(200);
        expect(r.body.error.code).toEqual(101);
        expect(r.body.error.type).toEqual('missing_access_key');

    });

});

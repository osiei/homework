const mail = {
    positiveMail: 'support@apilayer.com',
};
const mailArray = [['support@apilayer.com', true],
    ['123@test.ru', true],
    ['Qa_23@sd.com', true],
    ['.com', false],
    ['1@2', false],
    ['T', false],];

export { mail, mailArray };
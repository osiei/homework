import supertest from 'supertest';
import {user,urls} from '../config/index';

const CheckEmail = function CheckEmail() {
    this.checkEmailmethod = async function checkEmailmethod(mail, key) {

        const request = supertest(urls.mailboxlayer);
        const r = await request.get('')
            .query({ email: mail, access_key: key });
        return r;
    };
};

export { CheckEmail };